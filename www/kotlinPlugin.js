var exec = require('cordova/exec');

var KotlinPlugin = (function () {
    function KotlinPlugin() {
    }
    KotlinPlugin.hello = function (input, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "KotlinPlugin", "hello", [input]);
    };
    return KotlinPlugin;
}());

module.exports = KotlinPlugin;
